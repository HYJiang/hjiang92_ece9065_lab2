// configure routes
/*global MyApp*/
    myApp.config(function($routeProvider) {
       
        $routeProvider

             // route for the Login page
            .when('/', {
                templateUrl : "Views/login.html",
                controller  : "loginController"
            })

            // route for the Posting page
            .when('/Posting.html', {
                templateUrl:"Views/Posting.html",
                controller: 'postingController'
            })

            // route for the PostTracking page
            .when('/PostsTracking.html', {
                templateUrl:"Views/PostsTracking.html",
                controller  : 'postsTrackingController'
            })
            
            //route for the SpecificPostComments page
            .when('/SpecificPostComments.html',{
                templateUrl:"Views/SpecificPostComments.html",
                controller  : 'specificPostCommentsController'
            });
            
        

    });